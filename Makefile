#! /usr/bin/make -f

.PHONY: tests
tests:
	pytest-3

# run tests not requiring network
.PHONY: quicktests
quicktests:
	pytest-3 -m 'not network'

.PHONY: lint
lint:
	flake8 . bin/*

# download some data from the web for use in the package
.PHONY: cache-data
cache-data:
	python3 cache-data.py

# convert the cache data to make it importable
.PHONY: convert-data
convert-data:
	python3 convert-data.py

.PHONY: clean
clean:
	find . -type f -name '*.pyc' -delete
	find . -type d -name '__pycache__' -delete
	rm -f reportbug/data.py
	rm -rf build
	rm -rf reportbug.egg-info
